import React, { useState } from "react";
import {
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { currencies } from "utils/stubs";
import style from "./_currencySwitch.module.scss";
const CurrencySwitch = ({ pair, switchCurrency }) => {
  const [dropdownOpen, setOpen] = useState(false);
  const toggle = () => setOpen(!dropdownOpen);
  return (
    <ButtonDropdown isOpen={dropdownOpen} toggle={toggle} color="primary">
      <DropdownToggle caret color="primary">
        {pair}
      </DropdownToggle>
      <DropdownMenu>
        {currencies.map((item) => (
          <DropdownItem
            key={item}
            className={pair === item ? style.selected : ""}
            onClick={() => {
              pair !== item && switchCurrency(item);
            }}
          >
            {item}
          </DropdownItem>
        ))}
      </DropdownMenu>
    </ButtonDropdown>
  );
};

export default CurrencySwitch;
