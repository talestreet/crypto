import React from "react";
import { Row, Col, Progress } from "reactstrap";
import moment from "moment";
const CurrencyPair = ({
  exchange,
  high = 0,
  low = 0,
  vwap = 0,
  pair,
  switchCurrency,
  timestamp,
}) => {
  const percentage = Math.floor(((vwap - low) * 100) / (high - low));
  return (
    <div>
      <div>
        {" "}
        <small>{low}</small> <small className="float-right">{high}</small>
      </div>
      <Progress multi>
        <Progress bar color="danger" value={percentage} />
        <Progress bar color="warning" value={100 - percentage} />
      </Progress>
      <div className="text-muted">
        {" "}
        <small>low</small> <small className="float-right">high</small>
      </div>
      <hr />
      <Row className="mb-1">
        <Col sm="4" xs="1" className="text-secondary">
          <i className="fas fa-university"></i>{" "}
          <span className="d-none d-sm-inline">Exchange</span>
        </Col>
        <Col sm="1" className="d-none d-sm-block">
          :
        </Col>
        <Col className="text-capitalize text-primary">{exchange}</Col>
      </Row>
      <Row className="mb-1">
        <Col sm="4" xs="1" className="text-secondary">
          <i className="fas fa-dollar-sign"></i>{" "}
          <span className="d-none d-sm-inline">Price</span>{" "}
        </Col>
        <Col sm="1" className="d-none d-sm-block">
          :
        </Col>
        <Col className="text-primary">{vwap}</Col>
      </Row>
      <Row className="text-muted mt-3">
        <Col sm="12">
          <small>
            <i className="far fa-clock"></i> Last Updated at{" "}
            {moment(timestamp).format("h:mm:ss a")}
          </small>
        </Col>
      </Row>
    </div>
  );
};

export default CurrencyPair;
