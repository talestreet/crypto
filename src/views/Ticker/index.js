import React, { useEffect, useState, useRef } from "react";
import style from "./_ticker.module.scss";
import CurrencyPair from "components/CurrencyPair";
import { Card, CardHeader, CardBody } from "reactstrap";
import CurrencySwitch from "components/CurrencySwitch";
const Ticker = () => {
  const [pair, setPair] = useState("btcusd");
  const [data, setData] = useState(null);
  const [status, setStatus] = useState("connecting");
  const socket = useRef(); //to persist across renders
  const switchCurrency = (currency) => {
    setData(null);
    sendMessage("unsubscribe", pair);
    sendMessage("subscribe", currency);
    setPair(currency);
  };
  const sendMessage = (type, pair) => {
    socket.current.send(
      JSON.stringify({ type, feeds: [`ticker.sfox.${pair}`] })
    );
  };
  const init = () => {
    socket.current = new WebSocket("wss://ws.sfox.com/ws");
    socket.current.onopen = function (data) {
      setStatus("connected");
      sendMessage("subscribe", pair);
    };
    socket.current.onmessage = (e) => {
      const data = JSON.parse(e.data);
      if (data.payload) {
        setData(data.payload);
      }
    };
    socket.current.onerror = function (error) {
      setStatus("error");
    };
  };
  useEffect(() => {
    init();
    return function () {
      socket.current.close();
    };
  }, []);
  const connecting = status === "connecting";
  const listeningAndEmpty = status === "connected" && !data;
  const error = status === "error" ? "Something went wrong" : null;
  return (
    <Card className={style.root}>
      <CardHeader>
        <CurrencySwitch switchCurrency={switchCurrency} pair={pair} />
      </CardHeader>
      <CardBody>
        {status === "error" && "Something went wrong."}
        {connecting && "Connecting..."}
        {listeningAndEmpty && "Waiting for data..."}
        {!connecting && !listeningAndEmpty && !error && (
          <CurrencyPair {...data} switchCurrency={switchCurrency} pair={pair} />
        )}
      </CardBody>
    </Card>
  );
};

export default Ticker;
